from django.contrib import admin
from app_pessoa.models import Pessoa

class Pessoas(admin.ModelAdmin):
    list_display = ('id','nome','telefone','email')
    list_display_links = ('id','nome')
    search_fields = ('nome',)

admin.site.register(Pessoa, Pessoas)