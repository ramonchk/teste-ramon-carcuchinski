from django.db.models.signals import post_save
from django.dispatch import receiver
from app_pessoa.models import Pessoa
import requests
@receiver(post_save, sender=Pessoa)
def my_handler(sender, **kwargs):
    if 'instance'in kwargs:
        post_data = {"nome": kwargs['instance'].nome, "telefone": kwargs['instance'].telefone, "email": kwargs['instance'].email}
        response = requests.post('http://0.0.0.0:7001/api/garagens/', data=post_data)
        content = response.content
        print (content)
