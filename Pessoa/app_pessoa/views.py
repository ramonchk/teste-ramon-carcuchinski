from rest_framework import viewsets
from app_pessoa.models import Pessoa
from app_pessoa.serializer import PessoaSerializer

class PessoasViewSet(viewsets.ModelViewSet):
    queryset = Pessoa.objects.all()
    serializer_class = PessoaSerializer
