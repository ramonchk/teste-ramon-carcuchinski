from django.apps import AppConfig


class AppPessoaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_pessoa'
    def ready(self):
        import app_pessoa.signals