from django.apps import AppConfig


class AppGaragemConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_garagem'
