from rest_framework import serializers
from app_garagem.models import Garagem, Veiculo

class GaragemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garagem
        fields = ['id','email','telefone','veiculo_set']

class VeiculoSerializer(serializers.ModelSerializer):
    cond_props = serializers.SerializerMethodField()

    class Meta:
        model = Veiculo
        fields = '__all__'

    def get_cond_props(self, obj):
        return obj.get_prop_and_date()
    