from django.contrib import admin
from app_garagem.models import Garagem, Veiculo

class Veiculos(admin.ModelAdmin):
    list_display = ('id','cor','data_fab','modelo','is_moto')
    list_display_links = ('id','modelo')
    search_fields = ('modelo',)
class Garagens(admin.ModelAdmin):
    list_display = ('id','email','telefone')
    list_display_links = ('id','email')
    search_fields = ('modelo',)
admin.site.register(Garagem, Garagens)
admin.site.register(Veiculo, Veiculos)