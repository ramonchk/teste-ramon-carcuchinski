from django.db import models

class Garagem(models.Model):
    telefone = models.CharField(max_length=13)
    email = models.CharField(max_length=30)

    def __str__(self):
        return self.email

class Veiculo(models.Model):
    cor = models.CharField(max_length=18)
    modelo = models.CharField(max_length=20)
    data_fab = models.DateField()
    is_moto = models.BooleanField()
    garagem = models.ForeignKey(Garagem, on_delete=models.CASCADE)

    def __str__(self):
        return (f"{self.cor} - {self.data_fab}") if not self.is_moto else (f"{self.modelo} - {self.data_fab}")
    
    def get_prop_and_date(self):
        return (self.cor, self.data_fab) if not self.is_moto else (self.modelo, self.data_fab)
