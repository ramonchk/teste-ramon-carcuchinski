from django.shortcuts import render
from rest_framework import viewsets
from app_garagem.models import Garagem, Veiculo
from app_garagem.serializer import GaragemSerializer, VeiculoSerializer

class GaragensViewSet(viewsets.ModelViewSet):
    queryset = Garagem.objects.all()
    serializer_class = GaragemSerializer


class VeiculosViewSet(viewsets.ModelViewSet):
    queryset = Veiculo.objects.all()
    serializer_class = VeiculoSerializer