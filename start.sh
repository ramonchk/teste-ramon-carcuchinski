docker-compose -f docker-compose.yml build
if [ $? -eq 0 ]; then
  echo ">>> BUILD OK <<<" 
else
  echo ">>> BUILD FAILED <<<"  >&2
fi

docker-compose -f docker-compose.yml up -d

if [ $? -eq 0 ]; then
  echo ">>> UP OK <<<" 
else
  echo ">>> UP FAILED <<<"   >&2
fi
# docker-compose -f docker-compose.yml up --build